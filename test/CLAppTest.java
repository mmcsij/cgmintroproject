import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doCallRealMethod;

public final class CLAppTest extends CLAppTestHelper{

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Mock
    private CLApp appMock;


    @Before
    public void init (){
        MockitoAnnotations.initMocks(this);
        doCallRealMethod().when(appMock).processInput(Mockito.anyString(), Mockito.anyMap());
        System.setOut(new PrintStream(outContent));
    }
    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void testPossibleQuestionsAndAnswersValidity(){
        Map<String, Boolean> testMap = new HashMap<String, Boolean>();
        populateMap(testMap);
        for (Map.Entry<String, Boolean> mapEntry : testMap.entrySet()) {
            boolean isValidInput = appMock.validateInput(mapEntry.getKey());
            assertEquals(mapEntry.getValue().booleanValue(), isValidInput);
        }
    }

    @Test
    public void testPositiveEvaluation() {
        Map<String, List<String>> testQAMap = new HashMap<String, List<String>>();
        String correctInput = "What's Peter's favourite food? \"pizza\" \"pasta\" \"pancake\"";
        boolean isValidInput = appMock.validateInput(correctInput);
        assertTrue(isValidInput);

        appMock.processInput(correctInput, testQAMap);
        assertEquals(1, testQAMap.size());
        assertTrue(testQAMap.containsKey("What's Peter's favourite food?"));
        assertTrue(testQAMap.containsValue(new ArrayList<String>(Arrays.asList("pizza", "pasta", "pancake"))));

        appMock.processInput("What's Peter's favourite food?", testQAMap);
        assertEquals("  - pizza\r\n  - pasta\r\n  - pancake\r\n", outContent.toString());
    }

    @Test
    public void testNegativeEvaluation() {
        Map<String, List<String>> testQAMap = new HashMap<String, List<String>>();
        String correctInput = "What's Peter's favourite food?";
        boolean isValidInput = appMock.validateInput(correctInput);
        assertTrue(isValidInput);

        appMock.processInput(correctInput, testQAMap);
        assertEquals(0, testQAMap.size());
        assertEquals("the answer to life, universe and everything is 42\r\n", outContent.toString());
    }

    @Test
    public void testInvalidInputEvaluation() {
        Map<String, List<String>> testQAMap = new HashMap<String, List<String>>();
        String correctInput = "What's Peter's favourite food";
        boolean isValidInput = appMock.validateInput(correctInput);
        assertFalse(isValidInput);

        appMock.processInput(correctInput, testQAMap);
        assertEquals(0, testQAMap.size());
        assertEquals("", outContent.toString());
    }

}
