import java.util.HashMap;
import java.util.Map;

public class CLAppTestHelper {

    public static String padText(String inputString, int length){
        if (null == inputString || length <= inputString.length()){
            return inputString;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append('x');
        }
        return inputString + sb.substring(inputString.length());
    }

    public static void populateMap(Map<String, Boolean> testMap){
        testMap.put("What's Peter's favourite food? \"pizza\" \"pasta\" \"pancake\"", Boolean.TRUE);
        testMap.put("What's Peter's favourite city", Boolean.FALSE);
        testMap.put("What's Peter's favourite animal? \"dog\" \"cat\" elephant", Boolean.FALSE);
        //Question exactly 255 long
        testMap.put(padText("Question1", 255) + "?", Boolean.TRUE);
        //Question longer than 255
        testMap.put(padText("Question2", 256) + "?", Boolean.FALSE);
        //Answer exactly 255 long
        testMap.put("Q3? \"a1\" \"" + padText("a2", 255) + "\"", Boolean.TRUE);
        //Answer longer then 255
        testMap.put("Q4? \"a4\" \"" + padText("a5", 256) + "\"", Boolean.FALSE);
    }
}
