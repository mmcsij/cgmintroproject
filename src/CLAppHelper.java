import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class CLAppHelper {

    private static final String ONLY_QUESTION_REGEX = "^.*\\?$";
    private static final String QUESTION_AND_ANSWERS_REGEX = "^(.*\\?) (\".*\")*$";
    protected static final String UNKNOW_QUESTION_MESSAGE = "the answer to life, universe and everything is 42";
    protected static final String INDENT = "  - ";
    private static final int INPUT_ELEMENT_MAX_LENGTH = 255;
    protected static final String INPUT_SEPARATOR = " ";
    private static final String QUESTION_TERMINATOR = "?";

    protected static boolean isOnlyQuestion(String input){
        return Pattern.compile(ONLY_QUESTION_REGEX).matcher(input).find();
    }

    protected static boolean hasAnswer(String input){
        return Pattern.compile(QUESTION_AND_ANSWERS_REGEX).matcher(input).find();
    }

    protected static boolean validateInput(String input){
        if (!isOnlyQuestion(input) && !hasAnswer(input)){
            return false;
        }
        List<String> strList = getAnswers(input);
        strList.add(getQuestion(input));
        return validateInputElementLength(strList);
    }

    private static boolean validateInputElementLength(List<String> inputElements){
        if (null != inputElements && inputElements.size() > 0){
            if (!inputElements.stream().allMatch(o -> o.replace("\"", "").replace("?", "").length() <= INPUT_ELEMENT_MAX_LENGTH)){
                return false;
            }
        }
        return true;
    }

    protected static String getQuestion(String input){
        return input.trim().substring(0, input.trim().indexOf(QUESTION_TERMINATOR) + 1);
    }

    protected static List<String> getAnswers(String input){
        List<String> result = new ArrayList<String>(Arrays.asList(input.trim().substring(input.trim().indexOf(QUESTION_TERMINATOR) + 1).trim().split(INPUT_SEPARATOR)));
        return result.stream().map(x -> x.replace("\"", "")).collect(Collectors.toList());
    }

}
