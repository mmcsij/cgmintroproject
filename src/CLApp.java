import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class CLApp extends CLAppHelper{

    public Map<String, List<String>> QAMap = new HashMap<String, List<String>>();

    public void execute(){
        Scanner console = new Scanner(System.in);

        String clInput = "";
        while (console.hasNextLine() && !"q".equalsIgnoreCase(clInput = console.nextLine())){
            evaluateInput(clInput, QAMap);
        }
    }

    public void evaluateInput(String clInput, Map<String, List<String>> qaMap){
       if (validateInput(clInput)) {
           processInput(clInput, qaMap);
       }
    }

    public void processInput(String input, Map<String, List<String>> qaMap){
        if (isOnlyQuestion(input)){
            qaMap.getOrDefault(input, Arrays.asList(UNKNOW_QUESTION_MESSAGE))
                    .stream().forEach(s -> {
                                            if (UNKNOW_QUESTION_MESSAGE.equals(s)) { System.out.println(s);
                                            } else { System.out.println(INDENT + s.replace("\"", "").trim()); }
                                           });
        }

        if (hasAnswer(input)){
            List<String> inputElements = getAnswers(input);
            if (null != inputElements && inputElements.size() > 0){
                for (String element : inputElements){
                    updateMap(getQuestion(input), getAnswers(input), qaMap);
               }
            }
        }
    }

    private void updateMap(String question, List<String> answers, Map<String, List<String>> qaMap){
        if (qaMap.containsKey(question.trim())){
            qaMap.replace(question.trim(), answers);
        } else {
            qaMap.put(question, answers);
        }
    }

}
